<?php

/**
 * Implement hook_views_data().
 */
function ajax_slideshow_views_data() {

    $data['node'] = array(
    'tab_title' => array(
      'title' => t('Tab Title'),
      'help' => t('Title and nid to be used by ajax_slideshow tabs.'),
      'field' => array(
        'field' => 'title',
        'handler' => 'views_handler_field_tab_title',
        'click sortable' => TRUE,
      ),
    ),
  );

  return $data;
}


/**
 * Implement hook_views_handler().
 *
 * @return array
 */
function ajax_slideshow_views_handlers() {
  $ret = array(
    'info' => array(
      'path' => drupal_get_path('module', 'ajax_slideshow') . '/includes',
    ),
    'handlers' => array(
      // field handlers
      'views_handler_field_tab_title' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );

  return $ret;
}

/**
 * Implement hook_views_plugins().
 */
function ajax_slideshow_views_plugins() {
  $plugins = array(
    'style' => array(
      'ajax_slideshow' => array(
        'title' => t('AJAX Slideshow'),
        'handler' => 'ajax_slideshow_style',
        'parent' => 'list',
        'theme' => 'views_ajax_slideshow',
        'theme path' => drupal_get_path('module', 'ajax_slideshow') .'/templates',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );

  return $plugins;
}
