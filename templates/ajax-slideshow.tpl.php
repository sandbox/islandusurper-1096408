<?php
/**
 * @file ajax-slideshow.tpl.php
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="ajax_slideshow">
<a class="prev"<?php echo $hide_nav_buttons; ?>>prev</a>
<div class="as-tabs-panes">
<div class="as-tabs"<?php echo $hide_tabs; ?>>
<div class="item-list">
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <<?php print $options['type']; ?>>
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes[$id]; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  </<?php print $options['type']; ?>>
</div>
</div>
<br style="clear:both"/>
<div class="as-panes<?php echo $show_border; ?>"></div>
</div>
<a class="next"<?php echo $hide_nav_buttons; ?>>next</a>
</div>
